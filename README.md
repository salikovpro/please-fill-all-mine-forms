# please-fill-all-mine-forms

### Configuration description

```js
[
    'https://getdisco.github.io/js-senior-test-task/pages/1.html', // URL with forms for autofill
    {
        container: '.output', // Container selector with forms for autofill
        buttons: '.form__actions', // Container selector for inserting autofill button

        iframeContainer: 'body', // selector inside iframe in case all fields placed in iframe

        fields: {
            name: '*[placeholder="Holder name"]', // name field selector
            number: '*[placeholder="Card number"]', // number field selector
            expiry: '*[placeholder="Expiry"]', // expiry field selector
            cvv: '*[placeholder="CVV/CVC"]', // cvv field selector
        },

    },
]
```
