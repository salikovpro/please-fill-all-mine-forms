const source = new Map([
    [
        'https://getdisco.github.io/js-senior-test-task/pages/1.html',
        {
            container: '.output',
            buttons: '.form__actions',

            fields: {
                name: '*[placeholder="Holder name"]',
                number: '*[placeholder="Card number"]',
                expiry: '*[placeholder="Expiry"]',
                cvv: '*[placeholder="CVV/CVC"]',
            },

        },
    ],

    [
        'https://getdisco.github.io/js-senior-test-task/pages/2.html',
        {
            container: 'body',
            buttons: '.form__actions',

            fields: {
                name: '*[placeholder="Holder name"]',
                number: '*[placeholder="Card number"]',
                expiry: '*[placeholder="Expiry"]',
                cvv: '*[placeholder="CVV/CVC"]',
            },

        },
    ],

    [
        'https://getdisco.github.io/js-senior-test-task/pages/3.html',
        {
            container: 'iframe',
            buttons: '.form__actions',

            iframeContainer: 'body',

            fields: {
                name: '*[placeholder="Holder name"]',
                number: '*[placeholder="Card number"]',
                expiry: '*[placeholder="Expiry"]',
                cvv: '*[placeholder="CVV/CVC"]',
            },

        },
    ],
]);

const values = {
    name: 'Ali Gasymov',
    number: '5403414685553195',
    expiry: '01/25',
    cvv: '123',
};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    switch (request.message) {
    case 'SETUP': {
        const config = source.get(sender.url);

        return sendResponse({ config, values });
    }

    default:
        return null;
    }
});
